#!/usr/bin/perl

# File: heatdnscache.pl
# Copyright Joey Kelly (joeykelly.net)
# August 23, 2017
# License: GPL version 2

# since we run our own BIND, and since some domains have short TTLs,
# let's heat our cache every half hour

# crontab entry:
# */30 * * * *  /home/oompaloompa/bin/heatdnscache.pl > /devnull

use strict;
use warnings;

use feature qw(switch say);

my @domain = qw(
  americanthinker.com
  lucianne.com
  slashdot.org
  soylentnews.org
  facebroke.com
);

foreach my $domain (@domain) {
  say     "dig \@localhost $domain";
  system  "dig \@localhost $domain";
}
